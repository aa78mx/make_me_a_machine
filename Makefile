all: get_roles configure play

play:
	ansible-playbook -i hosts playbook.yml

### run the playbook without sudo as root for the first run
root: get_roles playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml

### run a test playbook
one: get_roles configure
	ansible-playbook -i hosts tst.yml

### make dry run on all
test: get_roles configure
	ansible-playbook -i hosts playbook.yml --check --diff

### make a dry run on root
root_test: get_roles playbook_root.yml configure
	ansible-playbook -i hosts playbook_root.yml --check --diff

### make a dry run on one
one_test: get_roles configure
	ansible-playbook -i hosts tst.yml --check --diff

### populate the roles directory
get_roles:
	ansible-playbook -i hosts fetch.yml 

### create the root playbook
playbook_root.yml:
	sed -r 's/(become:).*/\1 no/' < playbook.yml > playbook_root.yml

### configure your run
configure: group_vars/all.yml

### generate group_vars
group_vars/all.yml:
	tools/configure

### remove the mess
clean:
	sed '/^ *- { name: /!d;s/^[^:]*: /roles\//;s/,.*//' fetch.yml | xargs rm -rf
	rm -f playbook_root.yml
